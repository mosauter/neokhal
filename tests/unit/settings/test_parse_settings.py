from pathlib import Path

from pytest import fixture

from neokhal import NeoKhalSettings


@fixture(name="config_path")
def config_path_fixture() -> Path:
    return Path(__file__).parent.joinpath("test_config.yaml")


def test_parse_settings_should_parse_yaml_file(config_path: Path) -> None:
    settings = NeoKhalSettings.parse(config_path)

    assert len(settings.databases) == 1
    assert settings.databases[0] == Path("~/.calendar/local_database")
