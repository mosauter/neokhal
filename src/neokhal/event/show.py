from datetime import datetime

from icalendar import Event


def view_event(event: Event) -> str:
    summary = event.get("summary")

    start: datetime = event.decoded("dtstart")
    end: datetime = event.decoded("dtend", default=start)
    start = start.astimezone(start.tzinfo)
    end = end.astimezone(end.tzinfo)

    return f"{start:%Y-%m-%dT%H:%M} - {end:%Y-%m-%dT%H:%M}: {summary}"
