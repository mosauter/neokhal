from argparse import ArgumentParser
from pathlib import Path

from icalendar import Calendar

from neokhal.event import view_event


def _parse() -> list[Path]:
    parser = ArgumentParser()
    parser.add_argument("calendar_files", nargs="+", type=Path)
    calendar_files: list[Path] = parser.parse_args().calendar_files

    return calendar_files


def show() -> None:
    calendar_files = _parse()

    for calendar_file in calendar_files:
        calendar = Calendar.from_ical(calendar_file.read_text())

        for event in calendar.walk("vevent"):
            print(view_event(event))
