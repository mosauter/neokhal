from __future__ import annotations

from pathlib import Path
from typing import NamedTuple

import yaml


class NeoKhalSettings(NamedTuple):
    databases: list[Path]

    @classmethod
    def parse(cls, config_file: Path) -> NeoKhalSettings:
        config_dict = yaml.safe_load(config_file.open())

        databases = [Path(database) for database in config_dict["databases"]]

        return cls(databases=databases)
