#!/bin/sh

poetry run pylint \
    --jobs 0 \
    --output-format "${1:-colorized}" \
    src
