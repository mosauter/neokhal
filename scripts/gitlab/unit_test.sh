#!/bin/sh

if [ "${1:-}" == "--coverage" ]; then
    coverage_options="--cov-branch --cov-report=term --cov-report=html --cov-report=xml --cov=src"
    shift
fi

if [ "${1:-}" == "--junit" ]; then
    junit_options="--junitxml xunit-reports/xunit-result-pytest.xml"
    shift
fi

# shellcheck disable=2086
poetry run pytest \
    --verbose \
    $coverage_options \
    $junit_options \
    tests/unit "$@"
